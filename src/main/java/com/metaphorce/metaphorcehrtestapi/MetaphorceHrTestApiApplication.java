package com.metaphorce.metaphorcehrtestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetaphorceHrTestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetaphorceHrTestApiApplication.class, args);
	}

}
