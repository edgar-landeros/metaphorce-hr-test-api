package com.metaphorce.metaphorcehrtestapi.	core.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="CONTRACT")
@Data
public class Contract {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name = "CONTRACTID")
	private Integer contractId;
	
	@ManyToOne(fetch= FetchType.LAZY)
	@JoinColumn(name="TAXTIDNUMBER")
	private Employee employee; 
	
	@ManyToOne(fetch= FetchType.LAZY)
	@JoinColumn(name="CONTRACTTYPEID")
	private ContractType contractType; 
	
	@Column(name = "DATEFROM", nullable = false)
	private LocalDateTime dateFrom;
	
	@Column(name = "DATETO", nullable = false)
	private LocalDateTime dateTo;
	
	@Column(name = "SALARYPERDAY", nullable = false)
	private Double salaryPerDay;
	
	@Column(name = "ISACTIVE", nullable = false)
	private Boolean isActive;
	
	@Column(name = "DATECREATED", nullable = false)
	private LocalDateTime dateCreated; 
}
