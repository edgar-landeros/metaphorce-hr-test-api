package com.metaphorce.metaphorcehrtestapi.core.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;



@Entity
@Table(name="EMPLOYEE")
@Data
public class Employee {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name = "TAXTIDNUMBER")
	private String taxtIdNumber;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@Column(name = "LASTNAME", nullable = false)
	private String lastName; 
	
	@Column(name = "BIRTHDATE", nullable = false)
	private LocalDate birthDate;
	
	@Column(name = "EMAIL", nullable = false)
	private String email;
	
	@Column(name = "CALLPHONE", nullable = false)
	private String callPhone;
	
	@Column(name = "ISACTIVE", nullable = false)
	private Boolean isActive;
	
	@Column(name = "DATECREATED", nullable = false)
	private LocalDateTime dateCreated;
	
}
