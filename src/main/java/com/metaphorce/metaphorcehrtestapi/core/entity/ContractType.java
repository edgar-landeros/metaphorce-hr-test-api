package com.metaphorce.metaphorcehrtestapi.core.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="CONTRACTTYPE")
@Data
public class ContractType {
	@Id
	@Column(name = "CONTRACTTYPEID")
	private Integer contractTypeId;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@Column(name = "DESCRIPTION", nullable = true)
	private String description;
	
	@Column(name = "ISACTIVE", nullable = false)
	private Boolean isActive;
	
	@Column(name = "DATECREATED", nullable = false)
	private LocalDateTime dateCreated;
}
