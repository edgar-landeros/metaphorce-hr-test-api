package com.metaphorce.metaphorcehrtestapi.core.service;

import java.util.List;

import com.metaphorce.metaphorcehrtestapi.core.dto.ActiveEmployeeDto;
import com.metaphorce.metaphorcehrtestapi.core.dto.EmployeeDto;

public interface EmployeeService {
	public EmployeeDto findByTaxtIdNumber(String taxtIdNumber) throws Exception;
	public List<ActiveEmployeeDto> getActiveEmployeeWithContractAndContractType();
}
