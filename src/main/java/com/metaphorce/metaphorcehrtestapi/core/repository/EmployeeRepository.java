package com.metaphorce.metaphorcehrtestapi.core.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.metaphorce.metaphorcehrtestapi.core.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String>{

	@Query("SELECT new com.metaphorce.metaphorcehrtestapi.core.dto.EmployeeDto(e.taxtIdNumber, e.name, e.lastName, e.birthDate, e.email, e.callPhone, e.isActive, e.dateCreated) "
			+ "FROM Employee e "
			+ "WHERE e.findByTaxtIdNumber = :findByTaxtIdNumber")
	public Employee findByTaxtIdNumber(String taxtIdNumber);
}
