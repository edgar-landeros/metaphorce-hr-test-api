package com.metaphorce.metaphorcehrtestapi.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.metaphorce.metaphorcehrtestapi.core.dto.ActiveEmployeeDto;
import com.metaphorce.metaphorcehrtestapi.core.entity.Contract;

@Repository
public interface ContractRepository extends JpaRepository<Contract, Long>{

	@Query("SELECT new com.metaphorce.metaphorcehrtestapi.core.dto.ActiveEmployeeDto (e.lastName + e.name, e.taxtIdNumber, e.email, ct.name, c.dateFrom, c.dateTo, c.salaryPerDay) "
			+ "FROM Contract c INNER JOIN c.employee e "
			+ "INNER JOIN c.contractType ct "
			+ "WHERE c.isActive = true AND ct.isActive = true AND e.isActive = true")
	public List<ActiveEmployeeDto> getActiveEmployeeWithContractAndContractType();
}
	