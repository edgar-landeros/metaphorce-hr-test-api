package com.metaphorce.metaphorcehrtestapi.core.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDto {

	private String taxtIdNumber;
	private String name;
	private String lastName; 
	private LocalDate birthDate;
	private String email;
	private String callPhone;
	private Boolean isActive;
	private LocalDateTime dateCreated;
}
