package com.metaphorce.metaphorcehrtestapi.core.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class ActiveEmployeeDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3894112857944964943L;
	
	private String fullName;
	private String taxtIdNumber;
	private String email;
	private String contractTypeName;
	private LocalDateTime contractDateFrom;
	private LocalDateTime contractDateTo;
	private Double contractSalaryPerDay;
	
	public ActiveEmployeeDto() {}
	
	public ActiveEmployeeDto(String fullName, String taxtIdNumber, String email, String contractTypeName, LocalDateTime contractDateFrom,
							LocalDateTime contractDateTo, Double contractSalaryPerDay) {
		this.fullName = fullName;
		this.taxtIdNumber = taxtIdNumber;
		this.email = email;
		this.contractTypeName = contractTypeName;
		this.contractDateFrom = contractDateFrom;
		this.contractDateTo = contractDateTo;
		this.contractSalaryPerDay = contractSalaryPerDay;
	}
}
