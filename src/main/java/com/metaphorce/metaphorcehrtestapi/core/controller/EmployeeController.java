package com.metaphorce.metaphorcehrtestapi.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.metaphorce.metaphorcehrtestapi.core.dto.ActiveEmployeeDto;
import com.metaphorce.metaphorcehrtestapi.core.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping(value = "find-active")
	public List<ActiveEmployeeDto> findActive() {
		return employeeService.getActiveEmployeeWithContractAndContractType();
	}
	
	
	@GetMapping(value = "local")
	public String local() {
		return "CONNECTED...";
	}
}
