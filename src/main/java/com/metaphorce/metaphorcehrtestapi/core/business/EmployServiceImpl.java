package com.metaphorce.metaphorcehrtestapi.core.business;

import java.util.List;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metaphorce.metaphorcehrtestapi.core.dto.ActiveEmployeeDto;
import com.metaphorce.metaphorcehrtestapi.core.dto.EmployeeDto;
import com.metaphorce.metaphorcehrtestapi.core.entity.Employee;
import com.metaphorce.metaphorcehrtestapi.core.repository.ContractRepository;
import com.metaphorce.metaphorcehrtestapi.core.repository.EmployeeRepository;
import com.metaphorce.metaphorcehrtestapi.core.service.EmployeeService;

@Service
public class EmployServiceImpl implements EmployeeService{

	@Autowired
	private ContractRepository contractRepository;
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	private Function <Employee,EmployeeDto> mapper = employee -> new EmployeeDto(employee.getTaxtIdNumber(), employee.getName(), employee.getLastName(),
						employee.getBirthDate(), employee.getEmail(), employee.getCallPhone(), employee.getIsActive(), employee.getDateCreated());
	
	@Override
	public List<ActiveEmployeeDto> getActiveEmployeeWithContractAndContractType() {
		return contractRepository.getActiveEmployeeWithContractAndContractType();
	}

	@Override
	public EmployeeDto findByTaxtIdNumber(String taxtIdNumber) throws Exception{	
		return mapper.apply(employeeRepository.findByTaxtIdNumber(taxtIdNumber));
	}

}
